let table = {
	element: null,

	init: function init(element_id) {
		table.element = document.getElementById(element_id);

		if (table.element) {
			table.clear();
		}

		return !!table.element;
	},
	ok: function ok() {
		if (!table.element) {
			console.log("ERROR: table.init() was not called or didn't succeed");
			return false;
		}

		return true;
	},
	clear: function clear() {
		if (!table.ok()) {
			return;
		}

		table.element.style.display = 'none';

		table.element.innerHTML = '';
	},
	add: function add(comment) {
		if (!table.ok()) {
			return;
		}

		let row_data = document.createElement('td');
		row_data.textContent = comment;
		let row = document.createElement('tr');
		row.appendChild(row_data);
		table.element.appendChild(row);

		table.element.style.display = '';
	},
};

let messages = {
	element: null,

	init: function init(element_id) {
		messages.element = document.getElementById(element_id);
		return !!messages.element;
	},
	ok: function ok() {
		if (!messages.element) {
			console.log("ERROR: messages.init() was not called or didn't succeed");
			return false;
		}

		return true;
	},
	clear: function clear() {
		if (!messages.ok()) {
			return;
		}

		messages.element.innerHTML = '';
	},
	add: function add(msg) {
		if (!messages.ok()) {
			return;
		}

		messages.element.innerHTML += (msg + '<br/>');
	},
};

function load() {
	table.clear();
	messages.clear();

	browser.tabs.executeScript(/*tabId=*/null, {file: '/injected.js'}).then(
		function success(data) {
			//messages.add('DEBUG: executeScript(): success');
		},
		function fail(error) {
			messages.add('Error injecting comment scraping script: ' + error);
		}
	);
}

document.addEventListener('DOMContentLoaded', function (event) {
	table.init('comments_table');
	messages.init('messages');

	browser.runtime.onMessage.addListener(function onMessage(message) {
		if (!message) {
			return;
		}

		if (message.command === 'sendComments') {
			table.clear();
			for (let c of message.data) {
				table.add(c);
			}
		}
		else if (message.command === 'print') {
			messages.add(message.data);
		}
	});
	/*
	browser.tabs.onActivated.addListener(function onActivated(activeInfo) {
		load();
	});
	*/
	browser.tabs.onUpdated.addListener(function onUpdated(tabId, changeInfo, tabInfo) {
		load();
	});
	load();
});

