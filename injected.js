function sendComments() {
	function getChildComments(node) {
		if (!node) {
			return [];
		}
			
		let result = [];
		for (let child of node.childNodes) {
			if (child.nodeType === Node.COMMENT_NODE) {
				let comment = child.nodeValue;
				if (comment && (comment.trim()!='')) {
					result.push(comment);
				}
			}

			result = result.concat(getChildComments(child));
		}

		return result;
	}

	browser.runtime.sendMessage({command: 'sendComments', data: getChildComments(document)}).then(
		function success(response) {
			console.log(response);
		},
		function fail(error) {
			console.log(error);
		}
	);
}

if ((document.readyState==='complete') || (document.readyState==='interactive')) {
	sendComments();
}
else {
	document.addEventListener('DOMContentLoaded', function onDOMContentLoaded(event) {
		sendComments();
	});
}

